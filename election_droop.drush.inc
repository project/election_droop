<?php
/**
 * @file
 * Drush commands for the Election Droop Integration module.
 */

/**
 * Implements hook_drush_command().
 */
function election_droop_drush_command() {
  $items = array();
  $items['election-droop'] = array(
    'description' => 'Counts the results of an election post using Droop. The report is printed to STDOUT.',
    'arguments' => array(
      'post_id' => 'The ID of the election post (required).',
      'method' => 'The count method (machine name). Run election-droop-methods for a list of available methods. Default: meek.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'required-arguments' => 1,
    'aliases' => array('edroop'),
    'examples' => array(
      'drush edroop 1 wigm' => 'Count the results of election post 1, using the Weighted Inclusive Gregory Method.',
    ),
  );
  $items['election-droop-methods'] = array(
    'description' => 'List the available methods for counting with Droop (see the election-droop command).',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('edroop-methods'),
  );
  return $items;
}

/**
 * Count the results for an election post.
 *
 * @param int $post_id
 *   The ID of the election post.
 * @param string $method
 *   The count method to use.
 */
function drush_election_droop($post_id, $method = 'meek') {
  // Load the post.
  $post = election_post_load($post_id);
  if (!$post) {
    return drush_set_error(dt('There are no election posts with the ID @id.', array('@id' => $post_id)));
  }
  $election = $post->election;
  // Check that it is sane to count results for this post.
  if (!election_droop_check_support($election)) {
    return drush_set_error(dt('This election type ("@type") does not support Droop.', array('@type' => $election->type_info['name'])));
  }
  // Check the method type.
  $allowed_methods = _election_droop_get_methods();
  if (!isset($allowed_methods[$method])) {
    return drush_set_error(dt('The count method "@method" was not found.', array('@method' => $method)));;
  }
  // Run the count.
  echo election_droop_count($post, $method);
}

/**
 * List the available counting methods.
 */
function drush_election_droop_methods() {
  $table = array();
  $table[] = array('Machine name', 'Description');
  foreach (_election_droop_get_methods() as $machine_name => $description) {
    $table[] = array($machine_name, $description);
  }
  return drush_print_table($table, TRUE);
}
